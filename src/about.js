import React, {Component, PureComponent} from 'react';
import {createPortal} from 'react-dom';
import {observer, inject} from 'mobx-react';

@inject('addstore','delStore',"rootstore")
@observer
export  default class MouseTracker extends Component {
    constructor(props) {
        super(props)


    }

    render() {
        //delStore
        const {addstore} = this.props;
        const {delStore} = this.props;
        const {rootstore} = this.props;
        console.log(this.props)

        return (
            <div>
                <h1>Move the mouse around!</h1>
                <button onClick={addstore.AddTodo}>按钮+</button>
                <button onClick={delStore.del}>按-</button>
                {rootstore.todos}
                <br/>
                <hr/>
                {rootstore.total}
            </div>
        );
    }
}