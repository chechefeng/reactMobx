import {addstore} from "./add"
import {delStore} from "./del"
import {rootstore} from "./root"
export   {
    addstore,
    delStore,
    rootstore
}