import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import DevTools from 'mobx-react-devtools'
import * as stores from './store';
import { Provider } from 'mobx-react'
console.log(...stores)
ReactDOM.render(<Provider   {...stores} >
    <div>
        <App/>
        <DevTools/>
    </div>

</Provider>, document.getElementById('root'));
registerServiceWorker();


